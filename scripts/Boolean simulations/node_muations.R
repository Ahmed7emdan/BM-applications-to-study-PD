
library(rJava)
library(RMut)
initJVM("8G")
library(ggplot2)
library(reshape2)
showOpencl()
setOpencl("gpu")

### Remote read
res <- read.csv("")

tca_net <- loadNetwork("bipartite_tca.sif")
# generate all possible initial-states each containing 10 Boolean nodes
set <- generateStates(tca, "100")
# generate all possible groups each containing a single node in the bipartite_tca network
tca <- generateGroups(tca_net, "all", 1, 0)

### Wrapper function
call_sensitivity <- function(network, set, param) {
  result <- calSensitivity(network, set, param)
  write.csv(result$Group_1,paste0(param,".csv"))
  df <- read.csv(paste0(param,".csv"))
  df <- subset(df, select = -c(X))
  df <- melt(df, id.vars="GroupID")
  # Separate plots
  # TODO: write to file, use a "pdf" interface
  ggplot(df, aes(GroupID,value)) + 
    geom_point() + 
    stat_smooth() +
    facet_wrap(~variable)
}

### Node-based mutations for the bipartite network
call_sensitivity(tca, set, "knockout")
call_sensitivity(tca, set, "state flip")
call_sensitivity(tca, set, "rule flip")
call_sensitivity(tca, set, "outcome shuffle")



#Node-based mutations for the bipartite network
knockout <- calSensitivity(tca, set, "knockout")
write.csv(knockout$Group_1,"knockout.csv")
df_knockout <-read.csv("knockout.csv")
df_knockout<- subset(df_knockout, select = -c(X))
df_knockout <- melt(df_knockout, id.vars="GroupID")
# Separate plots
ggplot(df_knockout, aes(GroupID,value)) + 
  geom_point() + 
  stat_smooth() +
  facet_wrap(~variable)


state_flip <- calSensitivity(tca,set, "state flip")
write.csv(state_flip$Group_1,"state_flip.csv")
df_state_flip  <-read.csv("state_flip.csv")
df_state_flip <- subset(df_state_flip, select = -c(X))
df_state_flip <- melt(df_state_flip, id.vars="GroupID")
# Separate plots
ggplot(df_state_flip, aes(GroupID,value)) + 
  geom_point() + 
  stat_smooth() +
  facet_wrap(~variable)


rule_flip <- calSensitivity(tca, set, "rule flip")
write.csv(rule_flip$Group_1,"rule_flip.csv")
df_rule_flip  <-read.csv("rule_flip.csv")
df_rule_flip  <- subset(df_rule_flip, select = -c(X))
df_rule_flip <- melt(df_rule_flip, id.vars="GroupID")
# Separate plots
ggplot(df_rule_flip, aes(GroupID,value)) + 
  geom_point() + 
  stat_smooth() +
  facet_wrap(~variable)

outcome_shuffle <- calSensitivity(tca, set, "outcome shuffle")
write.csv(outcome_shuffle$Group_1,"outcome_shuffle.csv")
df_outcome_shuffle  <-read.csv("outcome_shuffle.csv")
df_outcome_shuffle  <- subset(df_outcome_shuffle, select = -c(X))
df_outcome_shuffle <- melt(df_outcome_shuffle, id.vars="GroupID")
# Separate plots
ggplot(df_outcome_shuffle, aes(GroupID,value)) + 
  geom_point() + 
  stat_smooth() +
  facet_wrap(~variable)
