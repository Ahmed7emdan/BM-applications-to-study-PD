# Applications of Boolean modelling to study and stratify dynamics of a complex disease

Biological systems and diseases involve enormous and complex molecular pro-
cesses. Systems biology approaches help to decipher the complexities of disease
initiation and progression. One of these approaches is construction of molecular
interaction networks to visualise, analyse and interpret molecular mechanisms.
These networks can be used for building dynamic models to simulate disease
dynamics. Despite the lack of kinetic data in the networks, parameter-free mod-
els, e.g. Boolean models (BMs), can offer powerful analytical tools for dynamic
predictions. BMs can improve our understanding about the disease progression,
test research hypotheses, and identify therapeutic responses. We translate static
diagrams to dynamic models, verifying and validating their use in different scales
of complexities. We investigate the progression of Parkinson’s disease(PD) as an
example, and simulate the response to treatments. The BMs of the Parkinson’s
disease map are stratified based on omics datasets, identifying specific conditions
that may switch ON or OFF the disease endpoints. The cohort specific Boolean
models highlights different regulatory modules during disease progression.
## Data 
The data directory includes all the data we used in our analysis and enrichment. You can see a detailed explanation of each data  with direct hyperlink to it.
## Models
The model directory includes information about the different modelling formats. Further, it repressnts the initial states parameters with target activity probabilities and illustration of the configuration files which are essential to run the probabilistic Boolean models. It contains the biomolecules with specific probabilities.
## Results
The result directory includes multiple subdirectories, showing the results of each analysis we performed from the model verification to the cohort specific Boolean simulation. This part does not include the results from model construction. The model results of the model construction are found on the models directory.
## Scripts
The script directory includes the scripts we used in data wrangling, enrichment analysis, and Boolean simulation and includes essential pre-install guidlines for some tools.
