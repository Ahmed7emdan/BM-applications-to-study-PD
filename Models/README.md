# Boolean models formats, parameters, and configuration files 
## Formats 
Boolean models are constructed and represented using various modelling tools relying on different formats, including Simple Interaction Formats (SIF),
System Biology Markup Language qualitative (SBML-qual), BND format for MABOSS simulation and BNET for the BoolNet 
## Parameters
This repressnt the initial states parameters with target activity probabilities 
## Configuration files
These files are essential to run the probabilistic Boolean models. It contains the biomolecules with specific probabilities.
